#!/bin/bash
exec /var/www/nginx/fio/env/bin/gunicorn $1 \
--name "django" \
--workers 4 \
--timeout 180 \
--bind=unix:/tmp/fio.sock \
--limit-request-line 8190

