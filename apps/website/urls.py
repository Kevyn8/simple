# coding:utf-8
from website import views
from django.urls import include, path
from django.contrib.auth import views as auth_views

urlpatterns = [
    #path('', views.home, name='home'),
    path('api/simple/', include('simple.urls', namespace="simple")),
]
