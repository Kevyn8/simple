from .models import Category, Author, Book
from rest_framework import generics
from .serializers import CategorySerializer, AuthorSeriealizer, AuthorListSeriealizer, BookSerializer, \
    BookSerializerList, BookBaseSerializer
from rest_framework import filters
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework import status


# Author
class ActivitieView(generics.ListAPIView):
    serializer_class = CategorySerializer
    allowed_methods = ['GET']
    queryset = Category.objects.all()


class AuthorCreateView(generics.CreateAPIView):
    serializer_class = AuthorSeriealizer
    allowed_methods = ['POST', ]
    queryset = Author.objects.all()


class AuthorListView(generics.ListAPIView):
    serializer_class = AuthorListSeriealizer
    allowed_methods = ['GET', ]

    def get_queryset(self):
        return Author.objects.all().order_by('last_name')


class AuthorAllView(generics.ListAPIView):
    serializer_class = AuthorSeriealizer
    allowed_methods = ['GET', ]
    queryset = Author.objects.all()


# Book
class BookCreateView(generics.CreateAPIView):
    serializer_class = BookSerializer
    allowed_methods = ['POST', ]
    queryset = Book.objects.all()


class BookListDetailview(generics.ListAPIView):
    serializer_class = BookSerializerList
    allowed_methods = ['GET', ]

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Book.objects.filter(id=pk).order_by('last_name')


class BookListview(generics.ListAPIView):
    serializer_class = BookSerializerList
    allowed_methods = ['GET', ]
    queryset = Author.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'author__name', 'author__last_name']


class BookDeleteView(APIView):
    allowed_methods = ['DELETE']

    def get_queryset(self):
        return Book.objects.all()

    def get_object(self, pk):
        try:
            return Book.objects.get(pk=pk)
        except Book.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UpdateBook(generics.UpdateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookBaseSerializer

    def get_object(self, pk):
        try:
            return Book.objects.get(pk=pk)
        except Book.DoesNotExist:
            raise Http404

    def get_author(self, name):
        try:
            return Author.objects.get(name=name)
        except Book.DoesNotExist:
            raise Http404

    def update(self, request, *args, **kwargs):
        instance = self.get_object(kwargs.get('id'))
        instance.name = request.data.get("name")
        instance.author = self.get_author(request.data.get('author'))
        instance.save()

        serializer = self.get_serializer(instance)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)
