from django.urls import path
from django.conf.urls import include, url
from simple import view

app_name = 'simple'

urlpatterns = [
    path('listar-categorias/', view.ActivitieView.as_view(), name='category-list'),
    path('crear-autor/', view.AuthorCreateView.as_view(), name='create-autor'),
    path('listar-autor/', view.AuthorListView.as_view(), name='list-autor'),
    path('todo-autor/', view.AuthorAllView.as_view(), name='all-autor'),
    path('crear-libro/', view.BookCreateView.as_view(), name='create-box'),
    path('listar-libro-detalle/<int:id>/', view.BookListDetailview.as_view(), name='list-box'),
    path('listar-libro/<int:id>/', view.BookListview.as_view(), name='list-box'),
    path('borrar-libro/<int:id>/', view.BookDeleteView.as_view(), name='delete-box'),
    path('actualizar-libro/<int:id>/', view.UpdateBook.as_view(), name='update-box'),
]
