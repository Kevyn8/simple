from rest_framework import serializers
from .models import Category, Author, Book


class CategorySerializer(serializers.Serializer):
    class Meta:
        model = Category
        fields = "__all__"


class AuthorSeriealizer(serializers.Serializer):
    name = serializers.CharField(
        label=u'Name'
    )

    last_name = serializers.CharField(
        label=u'Apellido'
    )

    age = serializers.IntegerField(
        label=u'Edad',
        min_value=1
    )

    def create(self, validated_data):
        return self.Meta.model.objects.create(name=validated_data['name'],
                                              last_name=validated_data['last_name'],
                                              age=validated_data['age'])

    class Meta:
        model = Author
        fields = "__all__"


class AuthorListSeriealizer(serializers.Serializer):
    class Meta:
        model = Author
        fields = ('id', 'name',)


class BookSerializer(serializers.Serializer):
    name = serializers.CharField(
        label=u'Nombre'
    )

    editorial_name = serializers.CharField(
        max_length=20,
        label=u'Nombre Editorial'
    )

    author = serializers.ChoiceField(
        choices=Author.objects.all()
    )

    category = serializers.ChoiceField(
        choices=Category.objects.all()
    )

    def create(self, validated_data):
        return self.Meta.model.objects.create(name=validated_data['name'],
                                              editorial_name=validated_data['editorial_name'],
                                              author=validated_data['author'],
                                              category=validated_data['category'])

    class Meta:
        model = Book
        fields = "__all__"


class BookSerializerList(serializers.Serializer):
    class Meta:
        model = Book
        fields = ('author_name', 'author_last_name', 'category')


class BookBaseSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        super(BookBaseSerializer, self).__init__(*args, **kwargs)
        instance = self.instance

    name = serializers.CharField(
        label=u'Nombre'
    )

    author = serializers.ChoiceField(
        choices=Author.objects.all()
    )

    class Meta:
        model = Book
        fields = "__all__"
