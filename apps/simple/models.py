from django.db import models


class CommonStructure(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Fecha de actualización')

    def __str__(self):
        return f'Estructura Base {self.id}'

    class Meta:
        managed = False
        abstract = True
        verbose_name = u'Estructura Base'
        verbose_name_plural = u'Estructura Base'


class Category(CommonStructure):
    name = models.CharField(
        max_length=21,
        verbose_name=u'Nombre'
    )

    def __str__(self):
        return f'Categoria {self.id}'

    class Meta:
        verbose_name = u'Categoria'
        verbose_name_plural = u'Categorias'


class Author(CommonStructure):
    name = models.CharField(
        max_length=20,
        verbose_name=u'Nombre'
    )

    last_name = models.CharField(
        max_length=20,
        verbose_name=u'Apellido'
    )

    age = models.PositiveIntegerField(
        verbose_name=u'Edad',
        default=0
    )

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = u'Autor'
        verbose_name_plural = u'Autores'


class Book(CommonStructure):
    name = models.CharField(
        max_length=20,
        verbose_name=u'Nombre'
    )

    editorial_name = models.CharField(
        max_length=20,
        verbose_name=u'Apellido'
    )

    author = models.ForeignKey(
        Author,
        on_delete=models.PROTECT,
        verbose_name=u'Autor',
    )

    category = models.ForeignKey(
        Category,
        on_delete=models.PROTECT,
        verbose_name=u'Categoria',
        null=True
    )

    def __str__(self):
        return f'Autor {self.name}'

    class Meta:
        verbose_name = u'Autor'
        verbose_name_plural = u'Autores'
