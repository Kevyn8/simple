import string
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from celery import shared_task
@shared_task
def create_random_user_account(total):
    for num in range(total):
        username = 'user_{}'.format(get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username) #f'{username}@example.com' #  'texto' + 'texto'
        password = get_random_string()
        User.objects.create_user(username=username,
                                 email=email,
                                 password=password)
        print('{} cuentas fueron creadas'.format(num))