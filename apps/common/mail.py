import requests
from celery import shared_task


# Envio de correo
@shared_task
def send_simple_message(subject, text, user_mail):
    return requests.post(
        "https://api.mailgun.net/v3/sandbox25dd784c3cdd40f2af834826f2ae311d/messages",
        auth=("api", "c7c60a4de3745b6423b0c2313c5394bd-6f4beb0a-3c8a291f"),
        data={"from": "Excited User <noreplay@area51.com>",
              "to": ["{}".format(user_mail), ],
              "subject": "{}".format(subject),
              "text": "{}".format(text)})
